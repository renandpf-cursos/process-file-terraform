provider "aws" {
  region     = "us-east-1"
}

module "bucket-input-file" {
  source = "./s3"
  name = "process-files-input-renandpf"
}
